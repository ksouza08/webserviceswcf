﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace TesteWCFJSON
{
    [DataContract]
    public class BalancoComercialPorContinente
    {
        [DataMember]
        public int AnoBase { get; set; }

        [DataMember]
        public string Continente { get; set; }
        
        [DataMember]
        public double ValorExportado { get; set; }

        [DataMember]
        public double ValorImportado { get; set; }
    }
}