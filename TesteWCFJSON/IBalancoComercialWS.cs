﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace TesteWCFJSON
{
    // OBSERVAÇÃO: Você pode usar o comando "Renomear" no menu "Refatorar" para alterar o nome da interface "IBalancoComercialWS" no arquivo de código e configuração ao mesmo tempo.
    [ServiceContract]
    public interface IBalancoComercialWS
    {
        [OperationContract]
        [WebGet(
                ResponseFormat = WebMessageFormat.Json,
                UriTemplate = "paises/{anoBase}")]
            List<BalancoComercialPorPais> ObterBalancoPaises(
                string anoBase);

        [OperationContract]
        [WebGet(
                ResponseFormat = WebMessageFormat.Json,
                UriTemplate = "continentes/{anoBase}")]
            List<BalancoComercialPorContinente> ObterBalancoContinentes(
                string anoBase);
    }
}
