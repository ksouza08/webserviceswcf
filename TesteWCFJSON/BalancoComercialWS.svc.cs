﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace TesteWCFJSON
{
    // OBSERVAÇÃO: Você pode usar o comando "Renomear" no menu "Refatorar" para alterar o nome da classe "BalancoComercialWS" no arquivo de código, svc e configuração ao mesmo tempo.
    // OBSERVAÇÃO: Para iniciar o cliente de teste do WCF para testar esse serviço, selecione BalancoComercialWS.svc ou BalancoComercialWS.svc.cs no Gerenciador de Soluções e inicie a depuração.
    public class BalancoComercialWS : IBalancoComercialWS
    {
        public List<BalancoComercialPorPais> ObterBalancoPaises(
            string anoBase)
        {
            int ano;
            if(String.IsNullOrWhiteSpace(anoBase) || !int.TryParse(anoBase, out ano))
            {
                throw new FaultException(
                    "É obrigatório informar um ano-base válido!");
            }

            if (ano == 2016)
                return SimulacaoBalanco2016.ObterBalancoPaises();

            else if (ano == 2017)
                return SimulacaoBalanco2017.ObterBalancoPaises();

            else
            {
                throw new FaultException(
                    "O ano-base informado é inválido!");
            }
        }

        public List<BalancoComercialPorContinente> ObterBalancoContinentes(
            string anoBase)
        {
            int ano;
            if (String.IsNullOrWhiteSpace(anoBase) || !int.TryParse(anoBase, out ano))
            {
                throw new FaultException("É obrigatório informar um ano-base válido!");
            }

            if (ano == 2016)
                return SimulacaoBalanco2016.ObterBalancoContinentes();

            else if (ano == 2017)
                return SimulacaoBalanco2017.ObterBalancoContinentes();

            else
            {
                throw new FaultException(
                    "O ano-base informado é inválido!");
            }
        }
    }
}
