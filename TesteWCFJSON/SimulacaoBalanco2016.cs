﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TesteWCFJSON
{
    public static class SimulacaoBalanco2016
    {
        public static List<BalancoComercialPorPais> ObterBalancoPaises()
        {
            List<BalancoComercialPorPais> dados = new List<BalancoComercialPorPais>();

            dados.Add(new BalancoComercialPorPais()
            {
                AnoBase = 2016,
                Pais = "Alemanha",
                Sigla = "DE",
                ValorExportado = 35.1,
                ValorImportado = 15.5
            });

            dados.Add(new BalancoComercialPorPais()
            {
                AnoBase = 2016,
                Pais = "Canadá",
                Sigla = "CA",
                ValorExportado = 15.6,
                ValorImportado = 4.9
            });

            dados.Add(new BalancoComercialPorPais()
            {
                AnoBase = 2016,
                Pais = "China",
                Sigla = "CN",
                ValorExportado = 45.3,
                ValorImportado = 30.2
            });

            dados.Add(new BalancoComercialPorPais()
            {
                AnoBase = 2016,
                Pais = "Estados Unidos",
                Sigla = "US",
                ValorExportado = 50.4,
                ValorImportado = 25.3
            });

            dados.Add(new BalancoComercialPorPais()
            {
                AnoBase = 2016,
                Pais = "Japão",
                Sigla = "JP",
                ValorExportado = 40.2,
                ValorImportado = 20.4
            });

            dados.Add(new BalancoComercialPorPais()
            {
                AnoBase = 2016,
                Pais = "Reino Unido",
                Sigla = "GB",
                ValorExportado = 30.9,
                ValorImportado = 9.7
            });

            return dados;
        }

        public static List<BalancoComercialPorContinente> ObterBalancoContinentes()
        {
            List<BalancoComercialPorContinente> dados = new List<BalancoComercialPorContinente>();

            dados.Add(new BalancoComercialPorContinente()
            {
                AnoBase = 2016,
                Continente = "América",
                ValorExportado = 66,
                ValorImportado = 30.2
            });

            dados.Add(new BalancoComercialPorContinente()
            {
                AnoBase = 2016,
                Continente = "Ásia",
                ValorExportado = 85.5,
                ValorImportado = 50.6
            });

            dados.Add(new BalancoComercialPorContinente()
            {
                AnoBase = 2016,
                Continente = "Europa",
                ValorExportado = 66,
                ValorImportado = 25.2
            });

            return dados;
        }
    }
}