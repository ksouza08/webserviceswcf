﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace TesteWCFJSON
{
    [DataContract]
    public class BalancoComercialPorPais
    {
        [DataMember]
        public int AnoBase { get; set; }

        [DataMember]
        public string Pais { get; set; }

        [DataMember]
        public string Sigla { get; set; }

        [DataMember]
        public double ValorExportado { get; set; }

        [DataMember]
        public double ValorImportado { get; set; }
    }
}